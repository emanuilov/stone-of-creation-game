// © 2016 CREATION.TEHR.EU by-nc-sa

#pragma once

#include "Engine/StaticMeshActor.h"
#include "DefaultPickupItem.generated.h"

/**
 * 
 */
UCLASS()
class STONEOFCREATION_API ADefaultPickupItem : public AStaticMeshActor
{
	GENERATED_BODY()

public:

	ADefaultPickupItem();

	UFUNCTION(BlueprintImplementableEvent)
		bool OnUsed(ACharacter* character);

	UFUNCTION(BlueprintImplementableEvent)
		bool StartFocusItem();

	UFUNCTION(BlueprintImplementableEvent)
		bool EndFocusItem();
	
	
};
