// © 2016 CREATION.TEHR.EU by-nc-sa

#pragma once

#include "GameFramework/Actor.h"
#include "EnemyIceZone.generated.h"

UCLASS()
class STONEOFCREATION_API AEnemyIceZone : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemyIceZone();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	
	
};
