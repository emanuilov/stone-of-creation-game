// © 2016 CREATION.TEHR.EU by-nc-sa

#pragma once

#include "GameFramework/HUD.h"
#include "MainHUD.generated.h"

/**
 * 
 */
UCLASS()
class STONEOFCREATION_API AMainHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	AMainHUD();
	virtual void DrawHUD();
	UFUNCTION(BlueprintCallable, Category = "LevelTransfer")
		void GoToMagicZone();
//	UFUNCTION(BlueprintImplementableEvent, Category = "LevelTransfer")
		void GoToAirZone();
//	UFUNCTION(BlueprintImplementableEvent, Category = "LevelTransfer")
		void GoToCityZone();
//	UFUNCTION(BlueprintImplementableEvent, Category = "LevelTransfer")
		void GoToIceZone();
//	UFUNCTION(BlueprintImplementableEvent, Category = "LevelTransfer")
		void GoToFireZone();
//	UFUNCTION(BlueprintCallable, Category = "LevelTransfer")
		void GoToBattleground();

		void StartNPCQuest();
		
		FCanvasIcon startNPCIcon;

		FCanvasIcon CenterDotIcon;

		void DrawDamage(float xLocation, float yLocation);

		float xLocation;
		float yLocation;

private:
	class UTexture2D* CrosshairTexture;
	void DrawCrosshair();
	
};
