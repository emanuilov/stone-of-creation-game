// © 2016 CREATION.TEHR.EU by-nc-sa

#pragma once

#include "Object.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "DatabaseClient.generated.h"

/**
 * 
 */
UCLASS()
class STONEOFCREATION_API UDatabaseClient : public UObject
{
	GENERATED_BODY()

public:
	/**
	* Constructor
	*
	* @param Verb - verb to use for request (GET,POST,DELETE,etc)
	* @param Payload - optional payload string
	* @param Url - url address to connect to
	* @param InIterations - total test iterations to run
	*/
	UDatabaseClient();

	void StartClient(const FString& InVerb, const FString& InPayload, const FString& InUrl);

	/**
	* Kick off the Http request for the test and wait for delegate to be called
	*/
	void Run(void);

	/**
	* Delegate called when the request completes
	*
	* @param HttpRequest - object that started/processed the request
	* @param HttpResponse - optional response object if request completed
	* @param bSucceeded - true if Url connection was made and response was received
	*/
	void RequestComplete(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded);

private:
	FString Verb;
	FString Payload;
	FString Url;

};
