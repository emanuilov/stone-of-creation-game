// © 2016 CREATION.TEHR.EU by-nc-sa

#pragma once

#include "GameFramework/Character.h"
#include "SCharacter.generated.h"

UCLASS()
class STONEOFCREATION_API ASCharacter : public ACharacter
{
	GENERATED_BODY()


		/** Camera boom positioning the camera behind the character */
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	/** Collection sphere */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* CollectionSphere;

	virtual void Tick(float DeltaSeconds) override;

public:
	ASCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	/** Accessor function for initial power */
	UFUNCTION(BlueprintPure, Category = "Power")
		float GetInitialPower();

	/** Accessor function for current power */
	UFUNCTION(BlueprintPure, Category = "Power")
		float GetCurrentPower();

	/**
	Function to update the character's power
	* @param PowerChange This is the amount to change the power by, and it can be positive or negative.
	*/
	UFUNCTION(BlueprintCallable, Category = "Power")
		void UpdatePower(float PowerChange);

	/** Use the actor currently in view (if derived from UsableActor) */
	UFUNCTION(BlueprintCallable, WithValidation, Server, Reliable, Category = PlayerAbility)
		virtual void Use();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Loading")
		bool LoadingMagicZone = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Loading")
		bool LoadingAirZone = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Loading")
		bool LoadingCityZone = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Loading")
		bool LoadingIceZone = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Loading")
		bool LoadingFireZone = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Loading")
		bool LoadingBattleground = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "NPCs")
		bool startNPC = false;

	void OnFire();

	void StopFire();

	void HideQuests();

	void OnStartSprint();

	void OnStopSprint();

	bool GetLoadingMagicZone();

	bool GetLoadingBattleground();

	bool GetLoadingFireZone();

	bool GetLoadingIceZone();

	bool GetLoadingCityZone();

	bool GetLoadingAirZone();

	bool TraceFromSelf(FHitResult& OutResult, const float TraceDistance, ECollisionChannel const CollisionChannel);

	UPROPERTY(EditDefaultsOnly, Category = DefaultClasses)
		TSubclassOf<class AExplosion> ExplosionClass;

	UParticleSystem* ParticleSystem;

	bool isInCombat = false;

	float xLocation;

	float yLocation;

	bool GetCombatStatus();

	float GetYLocation();

	float GetXLocation();

	bool testValue = false;

	bool GetTestValue();

protected:

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

	/** Called when we press a key to collect any pickups inside the CollectionSphere */
	UFUNCTION(BlueprintCallable, Category = "Pickups")
		void CollectPickups();

	/**The starting power level of our character */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Power", Meta = (BlueprintProtected = "true"))
		float InitialPower;

	/** Multiplier for character speed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Power", Meta = (BlueprintProtected = "true"))
		float SpeedFactor;

	/** Speed when power level = 0 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Power", Meta = (BlueprintProtected = "true"))
		float BaseSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Power", Meta = (BlueprintProtected = "true"))
		float SprintSpeed;


	UFUNCTION(BlueprintImplementableEvent, Category = "Power")
		void PowerChangeEffect();

	/** Get actor derived from UsableActor currently looked at by the player */
	class ADefaultPickupItem* GetUsableInView();

	/* Max distance to use/focus on actors. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		float MaxUseDistance;

	/* True only in first frame when focused on new usable actor. */
	bool bHasNewFocus;

	/* Actor derived from UsableActor currently in center-view. */
	ADefaultPickupItem* FocusedUsableActor;

private:
	/**Current power level of our character */
	UPROPERTY(VisibleAnywhere, Category = "Power")
		float CharacterPower;

	bool bPressedSprint = false;
	float SprintModifier;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
	/** Returns CollectionSphere subobject **/
	FORCEINLINE class USphereComponent* GetCollectionSphere() const { return CollectionSphere; }
	/** third person camera */
	UPROPERTY(VisibleDefaultsOnly, Category = Camera)
		UCameraComponent* ThirdPersonCamera;

	/** an arm for the third person camera*/
	UPROPERTY(VisibleDefaultsOnly, Category = Camera)
		USpringArmComponent* ThirdPersonCameraArm;
};