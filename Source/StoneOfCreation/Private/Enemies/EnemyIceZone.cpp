// © 2016 CREATION.TEHR.EU by-nc-sa

#include "StoneOfCreation.h"
#include "EnemyIceZone.h"


// Sets default values
AEnemyIceZone::AEnemyIceZone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnemyIceZone::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemyIceZone::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

