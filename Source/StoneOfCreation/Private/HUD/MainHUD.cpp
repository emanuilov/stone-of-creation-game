// © 2016 CREATION.TEHR.EU by-nc-sa

#include "StoneOfCreation.h"
#include "SCharacter.h"
#include "MainHUD.h"

AMainHUD::AMainHUD() {
	ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTextureObject(TEXT("/Game/Environment/Universal/Textures/PlayerCrosshair"));
	if (CrosshairTextureObject.Object) {
		CrosshairTexture = CrosshairTextureObject.Object;
	}

	static ConstructorHelpers::FObjectFinder<UTexture2D> CenterDot(TEXT("/Game/Environment/Universal/Textures/SplashScreen"));

	CenterDotIcon = UCanvas::MakeIcon(CenterDot.Object);

	static ConstructorHelpers::FObjectFinder<UTexture2D> centerNPC(TEXT("/Game/Environment/Universal/Textures/startQuest"));

	startNPCIcon = UCanvas::MakeIcon(centerNPC.Object);
}

void AMainHUD::DrawHUD() {
	Super::DrawHUD();

	ASCharacter* defaultCharacter = Cast<ASCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());

	if (CrosshairTexture) {
		DrawCrosshair();
	}
	
	if (defaultCharacter->GetLoadingMagicZone() == true) {
		GoToMagicZone();
	}

	if (defaultCharacter->GetLoadingBattleground() == true) {
		GoToBattleground();
	}

	if (defaultCharacter->GetLoadingFireZone() == true) {
		GoToFireZone();
	}

	if (defaultCharacter->GetLoadingIceZone() == true) {
		GoToIceZone();
	}

	if (defaultCharacter->GetLoadingCityZone() == true) {
		GoToCityZone();
	}

	if (defaultCharacter->GetLoadingAirZone() == true) {
		GoToAirZone();
	}

	if (defaultCharacter->GetCombatStatus() == true) {
		DrawDamage(Canvas->ClipX * 0.6f, Canvas->ClipY * 0.6f);
	}

}

void AMainHUD::DrawCrosshair(){
	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition((Center.X - (CrosshairTexture->GetSurfaceWidth() * 0.5)),
		(Center.Y - (CrosshairTexture->GetSurfaceHeight() * 0.5f)));

	// draw the crosshair
	FCanvasTileItem TileItem(CrosshairDrawPosition, CrosshairTexture->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;

	Canvas->DrawItem(TileItem);
}

void AMainHUD::DrawDamage(float xLocation, float yLocation) {
	DrawText("-40HP", FColor::Green, xLocation, yLocation, GEngine->GetMediumFont(), 1.0f, true);
}

void AMainHUD::GoToAirZone() {
	float CenterX = Canvas->ClipX / 2;
	float CenterY = Canvas->ClipY / 2;
	float CenterDotScale = 1.0f;

	Canvas->SetDrawColor(255, 255, 255, 255);
	Canvas->DrawIcon(CenterDotIcon,
		CenterX - CenterDotIcon.UL*CenterDotScale / 2.0f,
		CenterY - CenterDotIcon.VL*CenterDotScale / 2.0f, CenterDotScale * 1.0f);
	UGameplayStatics::OpenLevel(GetWorld(), "/Game/Environment/AirZone/Maps/AirZone");
}

void AMainHUD::GoToMagicZone() {
	float CenterX = Canvas->ClipX / 2;
	float CenterY = Canvas->ClipY / 2;
	float CenterDotScale = 1.0f;

	Canvas->SetDrawColor(255, 255, 255, 255);
	Canvas->DrawIcon(CenterDotIcon,
		CenterX - CenterDotIcon.UL*CenterDotScale / 2.0f,
		CenterY - CenterDotIcon.VL*CenterDotScale / 2.0f, CenterDotScale * 1.0f);
	UGameplayStatics::OpenLevel(GetWorld(), "/Game/Environment/MagicZone/Maps/MagicZone");
}

void AMainHUD::GoToCityZone() {
	float CenterX = Canvas->ClipX / 2;
	float CenterY = Canvas->ClipY / 2;
	float CenterDotScale = 1.0f;

	Canvas->SetDrawColor(255, 255, 255, 255);
	Canvas->DrawIcon(CenterDotIcon,
		CenterX - CenterDotIcon.UL*CenterDotScale / 2.0f,
		CenterY - CenterDotIcon.VL*CenterDotScale / 2.0f, CenterDotScale * 1.0f);
	UGameplayStatics::OpenLevel(GetWorld(), "/Game/Environment/CityZone/Maps/CityZone");
}

void AMainHUD::GoToIceZone() {
	float CenterX = Canvas->ClipX / 2;
	float CenterY = Canvas->ClipY / 2;
	float CenterDotScale = 1.0f;

	Canvas->SetDrawColor(255, 255, 255, 255);
	Canvas->DrawIcon(CenterDotIcon,
		CenterX - CenterDotIcon.UL*CenterDotScale / 2.0f,
		CenterY - CenterDotIcon.VL*CenterDotScale / 2.0f, CenterDotScale * 1.0f);
	UGameplayStatics::OpenLevel(GetWorld(), "/Game/Environment/IceZone/Maps/IceZone");
}

void AMainHUD::GoToFireZone() {
	float CenterX = Canvas->ClipX / 2;
	float CenterY = Canvas->ClipY / 2;
	float CenterDotScale = 1.0f;

	Canvas->SetDrawColor(255, 255, 255, 255);
	Canvas->DrawIcon(CenterDotIcon,
		CenterX - CenterDotIcon.UL*CenterDotScale / 2.0f,
		CenterY - CenterDotIcon.VL*CenterDotScale / 2.0f, CenterDotScale * 1.0f);
	UGameplayStatics::OpenLevel(GetWorld(), "/Game/Environment/FireZone/Maps/FireZone");
}

void AMainHUD::GoToBattleground() {
	float CenterX = Canvas->ClipX / 2;
	float CenterY = Canvas->ClipY / 2;
	float CenterDotScale = 1.0f;

	Canvas->SetDrawColor(255, 255, 255, 255);
	Canvas->DrawIcon(CenterDotIcon,
		CenterX - CenterDotIcon.UL*CenterDotScale / 2.0f,
		CenterY - CenterDotIcon.VL*CenterDotScale / 2.0f, CenterDotScale * 1.0f);
	UGameplayStatics::OpenLevel(GetWorld(), "/Game/Environment/Battleground/Maps/MainBattleground");
}

void AMainHUD::StartNPCQuest() {
	float CenterX = Canvas->ClipX / 2;
	float CenterY = Canvas->ClipY / 2;
	float CenterDotScale = 1.0f;

	Canvas->SetDrawColor(255, 255, 255, 255);
	Canvas->DrawIcon(startNPCIcon,
		CenterX - startNPCIcon.UL*CenterDotScale / 2.0f,
		CenterY - startNPCIcon.VL*CenterDotScale / 2.0f, CenterDotScale * 1.0f);
}