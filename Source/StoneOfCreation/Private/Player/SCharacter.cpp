// © 2016 CREATION.TEHR.EU by-nc-sa

#include "StoneOfCreation.h"
#include "Items/DefaultPickupItem.h"
#include "World/SGameMode.h"
#include "World/Explosion.h"
#include "HUD/MainHUD.h"
#include "SCharacter.h"

ASCharacter::ASCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

												// Create a follow camera
/*	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->AttachTo(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm*/

	ThirdPersonCameraArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("ThirdPersonCameraArm"));
	ThirdPersonCameraArm->TargetOffset = FVector(0.f, 0.f, 0.f);
	ThirdPersonCameraArm->SetRelativeLocation(FVector(-40.f, 0.f, 160.f));
	ThirdPersonCameraArm->SetRelativeRotation(FRotator(-10.f, 0.f, 0.f));
	ThirdPersonCameraArm->AttachTo(GetMesh()); // attach it to the third person mesh
	ThirdPersonCameraArm->TargetArmLength = 200.f;
	ThirdPersonCameraArm->bEnableCameraLag = false;
	ThirdPersonCameraArm->bEnableCameraRotationLag = false;
	ThirdPersonCameraArm->bUsePawnControlRotation = true; // let the controller handle the view rotation
	ThirdPersonCameraArm->bInheritYaw = true;
	ThirdPersonCameraArm->bInheritPitch = true;
	ThirdPersonCameraArm->bInheritRoll = false;

	ThirdPersonCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("ThirdPersonCamera"));
	ThirdPersonCamera->AttachTo(ThirdPersonCameraArm, USpringArmComponent::SocketName);
	ThirdPersonCamera->bUsePawnControlRotation = false; // the arm is already doing the rotation
	ThirdPersonCamera->FieldOfView = 90.f;

												   // Create the collection sphere
	CollectionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollectionSphere"));
	CollectionSphere->AttachTo(RootComponent);
	CollectionSphere->SetSphereRadius(200.f);

	//set a base power level for the character
	InitialPower = 20000000.f;
	CharacterPower = InitialPower;

	// set the dependence of the speed on the power level
	SpeedFactor = 0.75f;
	BaseSpeed = GetCharacterMovement()->MaxWalkSpeed;

	// set the pickup item vars
	MaxUseDistance = 800;
	bHasNewFocus = true;
}

//////////////////////////////////////////////////////////////////////////
// Input

void ASCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	// Set up gameplay key bindings
	check(InputComponent);
	InputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	InputComponent->BindAction("HideQuests", IE_Pressed, this, &ASCharacter::HideQuests);

	InputComponent->BindAction("Fire", IE_Pressed, this, &ASCharacter::OnFire);
	InputComponent->BindAction("Fire", IE_Released, this, &ASCharacter::StopFire);

	InputComponent->BindAction("Sprint", IE_Pressed, this, &ASCharacter::OnStartSprint);
	InputComponent->BindAction("Sprint", IE_Released, this, &ASCharacter::OnStopSprint);

	InputComponent->BindAction("Collect", IE_Pressed, this, &ASCharacter::Use);

	InputComponent->BindAxis("MoveForward", this, &ASCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ASCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &ASCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &ASCharacter::LookUpAtRate);

	// handle touch devices
	//InputComponent->BindTouch(IE_Pressed, this, &ASCharacter::TouchStarted);
	//InputComponent->BindTouch(IE_Released, this, &ASCharacter::TouchStopped);
}


/*
void ASCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	// jump, but only on the first touch
	if (FingerIndex == ETouchIndex::Touch1)
	{
	}
}

void ASCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	if (FingerIndex == ETouchIndex::Touch1)
	{
	}
}
*/
void ASCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ASCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ASCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		if (bPressedSprint) Value *= 1.5f;

		AddMovementInput(Direction, Value);
	}
}

void ASCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		if (bPressedSprint) Value *= 1.5f;

		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}


/*
Performs raytrace to find closest looked-at UsableActor.
*/
ADefaultPickupItem* ASCharacter::GetUsableInView()
{
	FVector camLoc;
	FRotator camRot;

	if (Controller == NULL)
		return NULL;

	Controller->GetPlayerViewPoint(camLoc, camRot);
	const FVector start_trace = camLoc;
	const FVector direction = camRot.Vector();
	const FVector end_trace = start_trace + (direction * MaxUseDistance);

	FCollisionQueryParams TraceParams(FName(TEXT("")), true, this);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = false;
	TraceParams.bTraceComplex = true;

	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByChannel(Hit, start_trace, end_trace, COLLISION_PROJECTILE, TraceParams);

	return Cast<ADefaultPickupItem>(Hit.GetActor());
}

void ASCharacter::CollectPickups()
{

}

// Reports starting power
float ASCharacter::GetInitialPower()
{
	return InitialPower;
}

// Reports current power
float ASCharacter::GetCurrentPower()
{
	return CharacterPower;
}

// called whenever power is increased or decreased
void ASCharacter::UpdatePower(float PowerChange)
{
/*	// change power
	CharacterPower = CharacterPower + PowerChange;
	// change speed based on power
	GetCharacterMovement()->MaxWalkSpeed = BaseSpeed + SpeedFactor * CharacterPower;
	// call visual effect
	PowerChangeEffect();*/
}


/*
Update actor currently being looked at by player.
*/
void ASCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (Controller && Controller->IsLocalController())
	{
		ADefaultPickupItem* usable = GetUsableInView();

		// End Focus
		if (FocusedUsableActor != usable)
		{
			if (FocusedUsableActor)
			{
				FocusedUsableActor->EndFocusItem();
			}

			bHasNewFocus = true;
		}

		// Assign new Focus
		FocusedUsableActor = usable;

		// Start Focus.
		if (usable)
		{
			if (bHasNewFocus)
			{
				usable->StartFocusItem();
				bHasNewFocus = false;
			}
		}
	}
}

/*
Runs on Server. Perform "OnUsed" on currently viewed UsableActor if implemented.
*/
void ASCharacter::Use_Implementation()
{
	ADefaultPickupItem* usable = GetUsableInView();
	if (usable)
	{
		usable->OnUsed(this);
	}
}

bool ASCharacter::Use_Validate()
{
	// No special server-side validation performed.
	return true;
}

bool ASCharacter::TraceFromSelf(FHitResult& OutResult, const float TraceDistance, ECollisionChannel const CollisionChannel) {
	APlayerController* Controller = Cast<APlayerController>(GetController());

	if (Controller) {
		FVector CameraLocation;
		FRotator CameraRotation;
		Controller->GetPlayerViewPoint(CameraLocation, CameraRotation);

		FVector const StartTrace = CameraLocation;
		FVector const ShootDirection = CameraRotation.Vector();
		FVector const EndTrace = StartTrace + ShootDirection + TraceDistance;

		FName traceFromSelf = FName(TEXT("TraceFromSelf"));

		FCollisionQueryParams TraceParams(traceFromSelf, true, this);

		bool bHitReturned = false;
		UWorld* const World = GetWorld();
		if (World) {
			bHitReturned = World->LineTraceSingleByChannel(OutResult, StartTrace, EndTrace, CollisionChannel, TraceParams);
		}

		return bHitReturned;
	}

	return false;
}

void ASCharacter::OnFire() {
	FHitResult HitResult(EForceInit::ForceInit);
	
	bool bTraceSuccess = TraceFromSelf(HitResult, 12000.0f, ECollisionChannel::ECC_WorldDynamic);
	if (bTraceSuccess) {
		UWorld* const World = GetWorld();
		if (World) {
			AActor* const HitActor = HitResult.GetActor();
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, &HitActor->GetName());
			if (HitActor) {
				bool bIsEnemy = true;
				if (bIsEnemy) {
					xLocation = HitActor->GetActorLocation().X;
					yLocation = HitActor->GetActorLocation().Y;
					isInCombat = true;
					AMainHUD* mainHUD = Cast<AMainHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
					mainHUD->DrawDamage(xLocation, yLocation);
					//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("This is an on screen message!"));
					/*ParticleSystem = CreateDefaultSubobject<UParticleSystem>(TEXT("/Game/Environment/Universal/Effects/P_Fire"));
					if (ParticleSystem != NULL)
					{
						UGameplayStatics::SpawnEmitterAttached(
							ParticleSystem, //UParticleSystem*
							HitActor->GetRootComponent(),
							TEXT("Spine"),
							HitActor->GetActorLocation(), //relative offset 
							HitActor->GetActorRotation(), //relative rotation
							EAttachLocation::KeepRelativeOffset,
							true //auto delete on completion
							);
					}*/
				}
			}
		}
	}
}

void ASCharacter::StopFire() {
	isInCombat = false;
}

bool ASCharacter::GetLoadingMagicZone() {
	return LoadingMagicZone;
}

bool ASCharacter::GetLoadingBattleground() {
	return LoadingBattleground;
}

bool ASCharacter::GetLoadingFireZone() {
	return LoadingFireZone;
}

bool ASCharacter::GetLoadingIceZone() {
	return LoadingIceZone;
}

bool ASCharacter::GetLoadingCityZone() {
	return LoadingCityZone;
}

bool ASCharacter::GetLoadingAirZone() {
	return LoadingAirZone;
}

void ASCharacter::OnStartSprint()
{
	bPressedSprint = true;
	GetCharacterMovement()->MaxWalkSpeed = BaseSpeed * 2.0f;
}

void ASCharacter::OnStopSprint() {
	bPressedSprint = false;
	GetCharacterMovement()->MaxWalkSpeed = BaseSpeed;
}

float ASCharacter::GetXLocation() {
	return xLocation;
}

float ASCharacter::GetYLocation() {
	return yLocation;
}

bool ASCharacter::GetCombatStatus() {
	return isInCombat;
}

bool ASCharacter::GetTestValue() {
	return testValue;
}

void ASCharacter::HideQuests(){
	startNPC = false;
}