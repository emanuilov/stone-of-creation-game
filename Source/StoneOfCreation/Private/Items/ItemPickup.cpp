// © 2016 CREATION.TEHR.EU by-nc-sa

#include "StoneOfCreation.h"
#include "ItemPickup.h"

//Set default values
AItemPickup::AItemPickup()
{
	GetMesh()->SetSimulatePhysics(true);

	//the base power level of the battery
	BatteryPower = 150.f;
}


void AItemPickup::WasCollected_Implementation()
{
	// Use the base pickup behavior
	Super::WasCollected_Implementation();
	// Destroy the battery
	Destroy();
}

// report the power level of the battery
float AItemPickup::GetPower()
{
	return BatteryPower;
}