// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

namespace UnrealBuildTool.Rules
{
	public class GoogleCardboard : ModuleRules
	{
		public GoogleCardboard(TargetInfo Target)
		{
			PrivateIncludePaths.AddRange(
				new string[] {
					"GoogleCardboard/Private",
                    "E:/Epic Games/4.11/Engine/Source/Runtime/Renderer/Private",
					// ... add other private include paths required here ...
				}
				);

			PrivateDependencyModuleNames.AddRange(
				new string[]
				{
					"Core",
					"CoreUObject",
					"Engine",
					"InputCore",
					"RHI",
					"RenderCore",
					"Renderer",
					"ShaderCore",
					"HeadMountedDisplay"
				}
				);
		}
	}
}
